package grofers.webtest;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrokenLinkTest {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();

		driver.get(args[0]);

		List<WebElement> list = driver.findElements(By.tagName("a"));

		// Using parallel stream to parallel read from the list.
		list.parallelStream().forEach(t -> {
			String uri = t.getAttribute("href");
			if (uri != null) {
				HttpURLConnection hurl;
				try {
					hurl = (HttpURLConnection) new URL(uri).openConnection();

					hurl.setRequestProperty("User-Agent",
							"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
					hurl.setRequestMethod("HEAD");
					hurl.connect();
					int status = hurl.getResponseCode();
					// Checking if status is > 400 then link is broken, return ok otheriwse.
					String message = status >= 400 ? uri + "is broken" : uri + " is ok";
					System.out.println(message + status);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});

	}

}
